Home Test for iOS developer
=======================================
Chào mừng các bạn đến với công ty Sapo.
Để giúp các bạn đánh giá chính xác năng lực của mình, đồng thời cũng giúp công ty xây dựng kế hoạch sử dụng, phát triển năng lực của các bạn, chúng tôi có một bài kiểm tra năng lực nhỏ. 

Một số lưu ý:
Các bạn có thể tham khảo tất cả các nguồn tài nguyên bạn có.
Vì bài Test được thiết kế cho nhiều Level khác nhau, bạn không nhất thiết phải hoàn thành được hết tất cả các bài Test.

## Register
Trong bài này, chúng ta sẽ mô phỏng thực hiện việc đăng ký sử dụng app.
* Hiển thị màn đăng ký với 3 trường. Username, Password, Repeat Password. 
* Kiểm tra các trường nhập vào và chuyển sang màn chọn Tỉnh thành
* Load dữ liệu từ plist lên để hiển thị danh sách các thành phố và các quận huyện.
* Chọn một tỉnh thành, app sẽ chuyển sang màn thứ 3 để hiển thị các quận huyện ở trong tỉnh thành đó.
* Back lại màn chọn tỉnh thành, cell được chọn lúc trước vẫn hiển thị ở trạng thái Selected.
* Lựa chọn giới tính bằng 1 combo 3 nút radio button: Nam, Nữ, Khác
* Sử dụng PickerView để lựa chọn tuổi. Bắt đầu từ 8 tuổi, kết thúc ở 80 tuổi. Tuổi mặc định là 25 tuổi.
* Hiển thị màn tổng kết: Tên, tuổi, giới tính, tỉnh thành, quận huyện. và Nút xác nhận Để login vào Màn Dashboard của app. 





